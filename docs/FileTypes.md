# File Type Documentation of GLOWOS

| Filetype | Description | Link |
| ------ | ------ | ------ |
|.APP | This filetype is used by the App Runtime and is an executeable Application-Format| [![Generic badge](https://img.shields.io/badge/GLOW-Link%20not%20available-red.svg)]()|
|.DBF | This filetype is used by the Database Runtime to store information and settings about an application or the system| [![Generic badge](https://img.shields.io/badge/GLOW-Link%20not%20available-red.svg)]()|
|.LIB | This filetype is a Library Package used by Apps and Scripts| [![Generic badge](https://img.shields.io/badge/GLOW-Link%20not%20available-red.svg)]()|
|.LANG | This filetype is a Language Strings File used by Applications. Depending on the language the Text will be pulled from this file.| [![Generic badge](https://img.shields.io/badge/GLOW-Link%20not%20available-red.svg)]()|

